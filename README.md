A sample spring boot application project using gradle version v6.8.3

It has one endpoint at /testEndpoint which can be accessed at http://localhost:8080/testEndpoint 

It also has one health check to verify whether applicaton in up and running at http://localhost:8080/actuator/health

GitLab ci-cd pipeline is created (which gets triggered as soon as a code commit is made to the repository ) to build an artifact and push the artifact to the Gitlab Maven registry and 
artifacts can be viewed at https://gitlab.com/syed-imran/sample-springboot-demo/-/packages


Deployment of sample-spring-boot application on docker and kubernetes platforms

########### Inside docker platform ############

1. With Docker run (imperative style)

Run below docker run command

     docker run -d -p 8080:8080 --name sample-spring-boot-app registry.gitlab.com/syed-imran/sample-springboot-demo:latest

2. With Docker-Compose (Declarative style)

Run below docker-compose command

     docker-compose up -d

3. With Docker-Swarm (Declarative style)

Run below docker stack deploy command

     docker stack deploy -c docker-compose.yaml (stack-name)

     Eg: docker stack deploy -c docker-compose.yaml dev-stage

Once deployed in any of the above way, hit this url http://Machine-IP:8080/testEndpoint


######## Inside Kubernetes cluster #################
1. With kubernetes run (imperative style)

Run below kubernetes run command

     kubectl run sample-spring-boot-demo --image=registry.gitlab.com/syed-imran/sample-springboot-demo:latest


To access app endpoint run below command and hit this url http://Machine-IP:8080/testEndpoint

     kubectl port-forward --address (Machine-IP) pod/sample-spring-boot-demo 8080:8080



2. With kubernetes apply/create (declarative style)

Go inside kubernetes directory

     cd sample-springboot-demo/kubernetes

Run below kubernetes apply command to deploy sample application

     kubectl apply -f sample-spring-boot-app.yaml

Get all resources running in default namespace
     kubectl get all

Run below kubernetes apply command to deploy nginx ingress controller

     kubectl apply -f nginx-ingress-controller.yaml

Run below kubernetes apply command to deploy nginx ingress resource to access sample springboot app

     kubectl apply -f nginx-ingress-resource.yaml

Now get Loadbalancer-IP assigned to this k8s cluster if it's running on cloud environment like GCP.AZURE,AWS by running below command

     kubectl get all -n ingress-nginx
 
To access app endpoint run below command and hit this url http://Loadbalancer-IP/testEndpoint

OR 

To access it locally in minikube run below command and hit this url http://Machine-IP:8080/testEndpoint

     kubectl port-forward --address (Machine-IP) deployment.apps/salesledz-sample-spring-boot-deployment 8080:8080

 OR
 
     kubectl port-forward --address (Machine-IP) service/salesledz-sample-spring-boot-svc  8080:8080
     

3. With Helm (Helm v3)
 
Run below helm install command
 
    helm install salesledz salesledz-helm-files/

To access it locally in minikube run below command and hit this url http://Machine-IP:8080/testEndpoint

     kubectl port-forward --address (Machine-IP) deployment.apps/sales-app 8080:8080

 OR
 
     kubectl port-forward --address (Machine-IP) service/sales-app  8080:8080
     

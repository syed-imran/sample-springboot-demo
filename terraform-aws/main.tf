terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  access_key = "AKIATJMJNQEIFICTHSBV"
  secret_key = "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"


  #access_key = "AWS_ACCESS_KEY_ID"
  #secret_key = "AWS_SECRET_ACCESS_KEY"

}

variable "DPASSWORD" {}

#variable "dpassword" {
#  type = string
#}

#variable "mypassword" {
#   default = "Hello@432123"
#}
resource "aws_instance" "Web" {
  ami = "ami-013f17f36f8b1fefb"
  instance_type = "t2.micro"
  availability_zone = "us-east-1a"
  key_name          = "ssh-keys"
  vpc_security_group_ids = [aws_security_group.allow_web.id]
  user_data = <<-EOF
                  #!/bin/bash
                  sudo apt update -y
                  sudo apt install apache2 -y
                  sudo systemctl start apache2
                  sudo apt install docker.io -y
                  sudo bash -c 'echo your very first web server > /var/www/html/index.html'
                  echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
                  sudo docker login --username=syed-imran@outlook.com --password="${var.DPASSWORD}" registry.gitlab.com               
                  sudo docker pull nginx:latest
                  sudo docker tag nginx:latest registry.gitlab.com/syed-imran/sample-springboot-demo:nginx-latest
                  sudo docker push registry.gitlab.com/syed-imran/sample-springboot-demo:nginx-latest
                  sudo docker pull registry.gitlab.com/syed-imran/sample-springboot-demo:latest-v1
                  sudo docker run -d -p 8080:8080 --name sample-spring-boot-app registry.gitlab.com/syed-imran/sample-springboot-demo:latest-v1
                  sleep 10
                  sudo docker exec sample-spring-boot-app curl -s http://localhost:8080/testEndpoint
                  EOF

  tags = {
   Name = "Web-Server"
  }
}

# # # 6. Create Security Group to allow port 22,80,443
  resource "aws_security_group" "allow_web" {
    name        = "allow_web_traffic"
    description = "Allow Web inbound traffic"
    vpc_id      = "vpc-df0000a4"

    ingress {
      description = "HTTPS"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
      description = "HTTP"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
      description = "SSH"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
      description = "App-port"
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }


    egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
      Name = "allow_web"
    }
  }


  resource "aws_key_pair" "ssh-demo" {
    key_name   = "ssh-keys"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDC041WYogCMyMSSo9HJrGe5Mp/cjD6oDkUXawRIZ1xuwJzN26NeTx5iSWmdvfdlBYPP4StKEo+zuhHYEpls6Kfru3aaenX7msH08ICAfIyyWgjxgsO+ouoUe89wgKEwcRFNwrZPATBqz2hdqXx8+1tZrWL1+Wk8YDjJuNMj/ruBzV+iMyqIJOWd8+kWnfwGnfXPSjILd4TcUPWhLYWEIeCfQcIgnWw/2J+hUpExHikZBtYuxIH+MgXS2yGv/ArGcSdd5iYv1qEf+ZzV+izU2H0kBxNX+tVbYD8ems+U3ZaSu9LBzAFue/2CciK6EvW/qpKxNi5YzywSA7PhHpUAlIGVfcVsSBDffuDGn//3UWAE/s5a+XxlhnfZYBUVNuJkmG4MYbwPd+hscP3NapPEdto546oS2fE6ymEKohoferlH2J94Ow38TLqazVoe47wlJKOY69+jvh04z4axL7QjWEy+2T48ERQcb7zI3cilyZGizRm2SjiEcinwL/5q8SrgvdWCn77NjspAOC5Zo5rwSyhJXqKB94AkQALTSXIi6LncKQgV/3ocMtMrt35Hb9Yb1yRD8OpszJMf2fRQmykKwdc4ZazhhaTAQ506LJ8Vc33Vpjrs93zxGfMfMhXQ6Q8mbBymUAXckrV/ZjqU3LTDsnT2Xmx83zHrOpAf89itZA9hw=="
  }

  output "server_private_ip" {
    value = aws_instance.Web.private_ip
  }

  output "server_id" {
    value = aws_instance.Web.id
  }
  output "server_public_ip" {
    value = aws_instance.Web.public_ip
  }


#!/bin/bash
if [[ -z $(git status -s) ]]
then
  echo "tree is clean"
else
  echo "tree is dirty, please commit changes before running this"
  git add -A
  git commit -m "updated terraform files"
#  exit
fi

echo "End of script"

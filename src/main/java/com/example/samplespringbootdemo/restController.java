package com.example.samplespringbootdemo;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class restController {
    @GetMapping("/testEndpoint")
    public String testMethod() {
        return "Successfully created sample application testEndpoint for demo purpose";
    }
}

package com.example.samplespringbootdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleSpringbootDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleSpringbootDemoApplication.class, args);
	}

}

FROM openjdk:8-jre
#FROM openjdk:8-jre AS builder


#VOLUME /tmp
WORKDIR /app

#ADD sample-springboot-demo-0.0.65-SNAPSHOT.jar sample-springboot-demo.jar

COPY sample-springboot-demo-0.0.6-SNAPSHOT.jar sample-springboot-demo.jar



#FROM openjdk:8-jre

#WORKDIR /test
#COPY --from=builder /app/ /test
EXPOSE 8080

ENTRYPOINT [ "sh", "-c", "java  -jar /app/sample-springboot-demo.jar" ]
